package com.origins.teamorigins;

import com.origins.teamorigins.dao.RoleRepository;
import com.origins.teamorigins.dao.UserRepository;
import com.origins.teamorigins.model.Role;
import com.origins.teamorigins.model.User;
import com.origins.teamorigins.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
@ImportResource("classpath:teamorigins-security.xml")
public class Application implements CommandLineRunner {
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }

    @Override
    public void run(String... args) throws Exception {
        roleRepository.save(new Role(1L, "ROLE_ADMIN"));
        roleRepository.save(new Role(2L, "ROLE_USER"));

        userService.save(new User(null, "+77052359050", "teamspirit@gmail.com", "1234", null, Arrays.asList()));
    }
}
