package com.origins.teamorigins.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CompanyOwnerForm {
    @NotNull
    @Size(min=5, max=50)
    private String name;

    @NotNull
    @Size(min=5, max=50)
    private String surname;

//    @NotNull
//    @Email
//    private String email;
//
    @NotNull
    @Size(min=3)
    private String industryName;

    @NotNull
    @Size(min=2, max=20)
    private String companyName;

    @NotNull
    @Size(min=5)
    private String description;

}
