package com.origins.teamorigins.form;

import com.sun.istack.internal.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Size;

@Data
public class UserForm {

    @NotNull
    @Size(min=10)
    private String phone;

    @NotNull
    @Email
    private String email;

    @NotNull
    @Size(min=8)
    private String password;

    @NotNull
    private String repeatedPassword;

}
