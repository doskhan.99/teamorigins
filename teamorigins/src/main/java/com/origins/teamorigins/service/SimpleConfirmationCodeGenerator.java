package com.origins.teamorigins.service;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
public class SimpleConfirmationCodeGenerator implements ConfirmationCodeGenerator {
    @Override
    public String generate(int length, boolean useLetters, boolean useNumbers) {
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }
}
