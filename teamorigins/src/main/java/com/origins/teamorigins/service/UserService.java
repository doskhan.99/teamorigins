package com.origins.teamorigins.service;

import com.origins.teamorigins.model.User;

import java.util.List;

public interface UserService {
    void save(User user);

    void update(User user);

    User findByEmail(String email);

    User findById(Long id);

    List<User> findAll();
}
