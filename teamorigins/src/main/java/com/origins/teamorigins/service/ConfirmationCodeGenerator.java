package com.origins.teamorigins.service;

public interface ConfirmationCodeGenerator {
    String generate(int length, boolean useLetters, boolean useNumbers);
}
