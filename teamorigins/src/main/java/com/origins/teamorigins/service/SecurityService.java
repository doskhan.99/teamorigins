package com.origins.teamorigins.service;

public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
