package com.origins.teamorigins.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SmsServiceProviderImpl implements SmsServiceProvider {

    @Value("${smsc_login}")
    private String apiUsername;

    @Value("${smsc_password}")
    private String apiPassword;

    @Value("${smsc_message}")
    private String smsToSend;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public String doGet(String phone, String codeToSend) {

        String message = String.format(smsToSend, codeToSend);

        String requiredUrl = String.format(
                "http://smsc.kz/sys/send.php?login=%s&psw=%s&phones=%s&mes=%s",
                this.apiUsername,
                this.apiPassword,
                phone,
                message);

        return restTemplate.getForEntity(requiredUrl,  String.class).toString();
    }
}
