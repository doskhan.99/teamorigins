package com.origins.teamorigins.service;

public interface SmsServiceProvider {
    String doGet(String phone, String codeToSend);
}
