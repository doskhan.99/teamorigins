package com.origins.teamorigins.service;

import com.origins.teamorigins.dao.CompanyRepository;
import com.origins.teamorigins.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public void save(Company company) {
        companyRepository.save(company);
    }

    public Company findById(Long id) {
        return companyRepository.findOne(id);
    }
}
