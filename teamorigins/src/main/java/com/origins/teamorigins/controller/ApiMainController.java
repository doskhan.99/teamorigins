package com.origins.teamorigins.controller;

import com.origins.teamorigins.model.Item;
import com.origins.teamorigins.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController()
public class ApiMainController {

    @Autowired
    private UserService userService;

    @PostMapping("api/companies/{companyId}/items/add")
    public List<Item> addItem(Authentication authentication,
                              @PathVariable("companyId") Integer companyId,
                              @RequestBody Map<String, String> values ) throws Exception {
        return new ArrayList<>();
    }
}
