package com.origins.teamorigins.controller;

import com.origins.teamorigins.dao.ConfirmationCodeRepository;
import com.origins.teamorigins.form.UserForm;
import com.origins.teamorigins.model.ConfirmationCode;
import com.origins.teamorigins.model.User;
import com.origins.teamorigins.service.ConfirmationCodeGenerator;
import com.origins.teamorigins.service.SmsServiceProvider;
import com.origins.teamorigins.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private ConfirmationCodeRepository confirmationCodeRepository;

    @Autowired
    private ConfirmationCodeGenerator generator;

    @Autowired
    private UserService userService;

    @Autowired
    private SmsServiceProvider smsServiceProvider;

    @GetMapping("register")
    public String register(UserForm userForm) {
        return "register";
    }

    @PostMapping("register")
    public String register(@Valid UserForm userForm, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "register";
        }

        String generatedCode = generator.generate(4, true, true);

        confirmationCodeRepository.save(new ConfirmationCode(null, generatedCode, userForm.getPhone()));

        System.out.println(smsServiceProvider.doGet(userForm.getPhone(), generatedCode));
        System.out.println(generatedCode);

        model.addAttribute("userForm", userForm);
        System.out.print("first");
        System.out.println(userService.findAll());

        return "confirmation_form";
    }

    @PostMapping("confirm")
    public String confirmRegistration(@RequestBody MultiValueMap<String, String> values, Model model) {

        List<String> generatedCode = values.get("code");

        ConfirmationCode code = confirmationCodeRepository.findByValue(generatedCode.get(0));

        if (code != null && !code.getPhone().isEmpty()) {

            User user = new User(null, values.get("phone").get(0), values.get("email").get(0),
                    values.get("password").get(0), null, Arrays.asList());

            userService.save(user);

            System.out.println(userService.findAll());


            return "success";

        } else {

            model.addAttribute("error", "true");

            return "confirmation_form";
        }
    }
}
