package com.origins.teamorigins.controller;

import com.origins.teamorigins.model.User;
import com.origins.teamorigins.service.SecurityService;
import com.origins.teamorigins.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public String index() {
        return "redirect:/home";
    }

    @GetMapping("/main")
    public String info() {
        return "redirect:/home";
    }

    @GetMapping("/home")
    public String homePage(Model model) {

        User user = null;
        String loggedInUsername = securityService.findLoggedInUsername();

        if (!loggedInUsername.equals("anonymousUser")) {
            user = userService.findByEmail(loggedInUsername);
            model.addAttribute("user", user);
        }

        return "home";
    }
}
