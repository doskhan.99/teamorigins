package com.origins.teamorigins.controller;

import com.origins.teamorigins.dao.ConfirmationCodeRepository;
import com.origins.teamorigins.form.CompanyOwnerForm;
import com.origins.teamorigins.model.Company;
import com.origins.teamorigins.model.ConfirmationCode;
import com.origins.teamorigins.model.Item;
import com.origins.teamorigins.model.User;
import com.origins.teamorigins.service.CompanyService;
import com.origins.teamorigins.service.ConfirmationCodeGenerator;
import com.origins.teamorigins.service.SmsServiceProvider;
import com.origins.teamorigins.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("owners")
public class CompanyOwnerController {

    @Autowired
    private UserService userService;

    @Autowired
    private CompanyService companyService;

    @PostMapping("{user_id}/add")
    public String addCompanyOwner(@PathVariable("user_id") Long user_id, @Valid CompanyOwnerForm companyOwnerForm, BindingResult bindingResult, Model model) {

        System.out.println(user_id);

        User user =  userService.findById(user_id);

        if (bindingResult.hasErrors()) {
            System.out.println("error");
            model.addAttribute("user", user);
            return "company_owner_form";
        }

        Set<Item> items = new HashSet<>();
        Company company = new Company(null, companyOwnerForm.getCompanyName(),
                companyOwnerForm.getIndustryName(), companyOwnerForm.getDescription(), items, user);

        companyService.save(company);

        user.getCompanyList().add(company);

        userService.save(user);

        return "redirect:/home";
    }

    @GetMapping("{user_id}/add")
    public String addCompanyOwner(@PathVariable("user_id") Long user_id, CompanyOwnerForm companyOwnerForm, Model model) {

        User user = userService.findById(user_id);
        model.addAttribute("user", user);

        return "company_owner_form";
    }

    @GetMapping("owners")
    public String getCompanies(@RequestParam("q") String username) {
        System.out.println(username);
        return null;
    }

}
