package com.origins.teamorigins.controller;

import com.origins.teamorigins.model.Company;
import com.origins.teamorigins.model.User;
import com.origins.teamorigins.service.CompanyService;
import com.origins.teamorigins.service.SecurityService;
import com.origins.teamorigins.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserService userService;

    @RequestMapping("companies/{id}")
    public String companyDetails(@PathVariable Long id, Model model) {

        Company company = companyService.findById(id);
        model.addAttribute("itemList", company.getItems());
        model.addAttribute("company", company);

        User user = userService.findByEmail(securityService.findLoggedInUsername());

        model.addAttribute("user", user);

        return "company_detail";
    }

    @GetMapping("companies/{id}/items/add")
    public String addItems(@PathVariable Long id, Model model) {

        model.addAttribute("company", companyService.findById(id));

        User user = userService.findByEmail(securityService.findLoggedInUsername());

        model.addAttribute("user", user);

        return "items_add";
    }
}
