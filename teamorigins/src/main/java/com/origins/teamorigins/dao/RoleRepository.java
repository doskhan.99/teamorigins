package com.origins.teamorigins.dao;

import com.origins.teamorigins.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}
