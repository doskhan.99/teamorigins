package com.origins.teamorigins.dao;

import com.origins.teamorigins.model.ConfirmationCode;
import org.springframework.data.repository.CrudRepository;

public interface ConfirmationCodeRepository extends CrudRepository<ConfirmationCode, Long>{
    ConfirmationCode findByValue(String value);
}
