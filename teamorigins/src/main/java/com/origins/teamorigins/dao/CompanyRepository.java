package com.origins.teamorigins.dao;

import com.origins.teamorigins.model.Company;
import org.springframework.data.repository.CrudRepository;

public interface CompanyRepository extends CrudRepository<Company, Long> {
}
