package com.origins.teamorigins.dao;

import com.origins.teamorigins.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmail(String email);
    User findById(Long id);
    List<User> findAll();
}